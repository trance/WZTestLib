Pod::Spec.new do |s|
s.name         = 'WZTestLib'
s.version      = '0.0.1'
s.summary      = '测试发布框架'
s.homepage     = 'https://gitee.com/trance/WZTestLib'
s.license      = { :type => 'MIT', :file => 'LICENSE' }
s.authors      = {'Trance' => '490359823@qq.com'}
s.platform     = :ios, '8.0'
s.source       = {:git => 'https://gitee.com/trance/WZTestLib.git', :tag => s.version}
s.source_files = 'Classes/**/*.{h,m}'
s.requires_arc = true
end
